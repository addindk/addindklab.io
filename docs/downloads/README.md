---
sidebar: auto
sidebarDepth: 0
---
# Downloads
Because of Windows security restrictions it is necessary to unblock all downloaded zip files before unzipping.
## ADDIN.DK Folder
[folder.zip](/folder.zip)

Unzip to c:\addin.dk

copy c:\addin.dk\appl\addin.dk.cfg to your bentley appl folder
## ADDIN.DK CONNECT / 2023 / 2024 and V8i
[addindk.zip](/addindk.zip)

Unzip to c:\addin.dk\programs
### 9/27/2022
- Bugfix for OpenRoads
### 1/26/2022
- Set leveloverride off by default
### 6/8/2021
- Matrikelsøgning updated to use dataforsyningen api
### 4/21/2021
- Use textstyle from dgnlib
### 2/2/2021
- Fix field Lodsejerliste
### 9/1/2021
- Fix theme in Lodsejerliste
### 12/21/2020
- Add support for TLS 1.2 in CONNECT
- Fix WFS feature property AreaType leading to incorrect number of geometries
- Add support for SamletFastejendom and Jordstykke from Datafordeleren in Lodsejerliste
### 12/18/2020
- Add support for TLS 1.2 in V8i
## Matrikelsøgning CONNECT / 2023 / 2024
[AIMatrikel.zip](/AIMatrikel.zip)
### 06/07/2024
- Skift tit Dataforsyningen Gsearch V2.0
### 11/23/2023
- Skift til Dataforsyningen Gsearch
### 08/29/2022
- Default Kommune can be defined in cfg variable AI_MATRIKEL_KOMMUNE = kommunekode
### 5/27/2021
- Fixed bug showing multipolygon
- Changed api to dataforsyningen
### 12/23/2020
- Add support for TLS 1.2
- Reset transient element on exit
- Breaking change; use token based login to Kortforsyningen. Set token in KF_TOKEN variable. 
## Adressesøgning CONNECT / 2023 / 2024
[AIAWS.zip](/AIAWS.zip)
### 08/12/2022
- Default Kommune can be defined in cfg variable AWS_KOMMUNE = kommunekode
### 07/28/2022
- Changed api from http://dawa.aws.dk to https://api.dataforsyningen.dk
### 12/23/2020
- Add support for TLS 1.2 in CONNECT
- Reset transient element on exit
## Ejendomsinformation CONNECT / 2023 / 2024
[AIEjendom.zip](/AIEjendom.zip)
### 03/01/2025
- Ændret url til matriklen2
### 04/11/2023
- Kan nu slå ejere op fra ejerregisteret
### 07/28/2022
- Changed api from http://dawa.aws.dk to https://api.dataforsyningen.dk
### 12/23/2020
- Add support for TLS 1.2
- Breaking change; use token based login to Kortforsyningen. Set token in KF_TOKEN variable. 
## Map Manager CONNECT / 2023 / 2024 
[AIKort.zip](/AIKort.zip)
### 01/30/2025
- Tilføjet danske koordinatsystem navne
## DHM CONNECT / 2023 / 2024 
[AIDHM.zip](/AIDHM.zip)
## WFS Booster CONNECT / 2023 / 2024
[AIWFS.zip](/AIWFS.zip)

Udpak zip fil og sæt MicroStation variabel MS_ADDINPATH > c:/addin.dk/AIWFS/

keyin mdl l map.addin.wfs;map.addin.wfs open
### 06/07/2024
- Rettet fejl valg af flueben ved model blev ikke gemt
- Tilføjet antal ud for hver WFS tjeneste i WFS Configuration Manager
- Tilføjet tykkere streg mellem kolonner i WFS Configuration Manager så det er nemmere at trække kolonne bredde.
### 02/05/2024
- Rettet fejl ved multiline
### 12/22/2023
- Rettet farver til RGB
- Tilføjet snap og locate lock på reference
### 12/18/2023
- Bruger nu kun MicroStations indbyggede transformationsfunktioner.
- Attributdata gemmes nu som items der kan ses i property panelet.
- Elementer oprettes nu med color by level, weight by level, style by level
- Annotation styres nu fra MicroStation Annotation scale lock.
- Rettet fejl ved multipolygon
- Rettet fejl hvor fil var sat til millimeter
- Rettet fejl ved skalering hvis fil ikke var sat til 1000 per meter i resolution
