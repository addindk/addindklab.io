---
sidebar: auto
---
# Products
All the listed products supports Bentley [MicroStation](https://www.bentley.com/en/products/brands/microstation) V8i and CONNECT.

Please contact us for more information about products and pricong options.
* E-mail: [rune@addin.dk](mailto:rune@addin.dk)
* Phone: [+45 2193 2703](tel:+4521932703)

## WFS Booster <badge text="AWARD" type="warning"/>
This is the fastest and most user-friendly WFS client for Bentley [MicroStation](https://www.bentley.com/en/products/brands/microstation) V8i and CONNECT.

::: warning AWARDS 
Bentley Be Inspired Finalist 2010 Innovation In Government
:::

### Highlights
* Supports WFS version 1.0.0, 1.1.0, 2.0.0
* Concurrent download from multiple services
* WFS 2.0.0 paging
* Thematic map based on attributtes
* Listview with zoom-to element
* Annotation Scale

<iframe width="740" height="555" src="https://www.youtube.com/embed/5Nd8DEcHr14" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## WFS Info
Tool to get attribute information from WFS elements.

## Map Manager
the best way to manage multiple WMS sources.