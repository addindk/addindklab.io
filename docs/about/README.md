# About
ADDIN.DK ApS was founded in 2012 by Rune Tvilum.

We are specialized in developing geospatial solutions for the Bentley platform and the web.

## Customers
We have many customers using our software:
* Municipalies
* Contractors
* Architects
* Chartered Surveyors
* Utilities

## Contact
Please contact us for more information.
* E-mail: [rune@addin.dk](mailto:rune@addin.dk)
* Phone: [+45 2193 2703](tel:+4521932703)