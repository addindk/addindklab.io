---
home: true
heroImage: /hero.png
tagline: Official homepage
actionText: Products →
actionLink: /products/
features:
- title: Bentley
  details: We develop add-ins for Bentley MicroStation, PowerCivil and more
- title: WFS Booster
  details: The fastest and most user-friendly WFS client for MicroStation
- title: Contact
  details: rune@addin.dk | +45 2193 2703
footer: ADDIN.DK ApS | Herredsvejen 14 | DK-3400 Hillerød | VAT no. DK-34884021
---